/*
	Project 2
	Name of Project: Marine vs Zerg
	Author: Sunghyun Yang
	Date: 2020.06.30
*/


//sprites
let spritesheet;
let speedspritesheet;
let playeranimation = [];
let speedanimation = [];
let player;
let speed;

let leaderBoard;
let scoreleaderBoard;
let roundleaderBoard;
let isleaderBoardOn;
let gameInfo;
let bullet1;
let bullet2;
let bullet3;
let scribble
let score1;
let score2;
let score3;

//preload images
function preload() {
  spritesheet = loadImage('/data/playerspritesheet.png');
  speedspritesheet = loadImage('/data/speedspritesheet.png');
  bullet1 = loadImage('/data/bullet1.png');
  bullet2 = loadImage('/data/bullet2.png');
  bullet3 = loadImage('/data/bullet3.png');
  moneyIcon = loadImage('/data/money.png');
  fontStyle = loadFont('/data/font.ttf');
}


function setup() 
{
	score1 = 1;
	score2 = 1;
	score3 = 1;
	scribble= new Scribble(); 
	isleaderBoardOn = 0;

	//jsonobject for communication
	gameInfo = {
	"playerSpeed":3,
	"playerHealth":200,
	"gameState":0,
	"round":1,
	"bullet1":40,
	"bullet2":3,
	"bullet3":3,
	"score":0,
	"money":500,
	"totalShots":0,
	"totalKills":0,
	}

	createCanvas(600, 400);

	//clickable button setup
	healthButton = new Clickable();
	healthButton.cornerRadius = 0;
	healthButton.strokeWeight = 2;
	healthButton.textFont = fontStyle;
	healthButton.x = 450;
	healthButton.y = 85;
	healthButton.width = 70;
	healthButton.height = 40;
	healthButton.onHover = function(){
		if(gameInfo.playerHealth <=199){
			this.color = "#E0D54B"
			this.textSize = 15
			this.text = "500$"
		} else {
			this.color = "#CC3232"
			this.textSize = 11
			this.text = "Max\nHealth"
		}
	}
	healthButton.onOutside = function(){
		this.color = "#7EB279"
		this.textSize = 11
		this.text="Gain\nHealth";
	}
	healthButton.onPress = function(){
		if(gameInfo.playerHealth <=199){
			this.color = "#D1C343"
			this.textSize = 11
			this.text = "500$"
		} else {
			this.color = "#CC3232"
			this.textSize = 11
			this.text = "Max\nHealth"
		}
	}
	healthButton.onRelease = function(){
		if(gameInfo.money >= 500 && gameInfo.playerHealth <=190){
			gameInfo.money = gameInfo.money - 500;
			gameInfo.playerHealth = 200;
			ws.send(JSON.stringify(gameInfo));
		}
	}

	speedButton = new Clickable();
	speedButton.cornerRadius = 0;
	speedButton.strokeWeight = 2;
	speedButton.textFont = fontStyle;
	speedButton.x = 450;
	speedButton.y = 135;
	speedButton.width = 70;
	speedButton.height = 40;
	speedButton.onHover = function(){
		if(gameInfo.playerSpeed <=4){
			this.color = "#E0D54B"
			this.textSize = 15
			this.text = "500$"
		} else {
			this.color = "#CC3232"
			this.textSize = 11
			this.text = "Max\nSpeed"
		}
	}
	speedButton.onOutside = function(){
		this.color = "#7EB279"
		this.textSize = 11
		this.text="Speed\nUpgrade";
	}
	speedButton.onPress = function(){
		if(gameInfo.playerSpeed <=5){
			this.color = "#D1C343"
			this.textSize = 11
			this.text = "500$"
		} else {
			this.color = "#CC3232"
			this.textSize = 11
			this.text = "Max\nSpeed"
		}
	}
	speedButton.onRelease = function(){
		if(gameInfo.money >= 500 && gameInfo.playerSpeed <=5){
			gameInfo.money = gameInfo.money - 500;
			gameInfo.playerSpeed ++;
			ws.send(JSON.stringify(gameInfo));
			speed.speed ++;
		}
	}

	bullet1Button = new Clickable();
	bullet1Button.cornerRadius = 0;
	bullet1Button.strokeWeight = 2;
	bullet1Button.textFont = fontStyle;
	bullet1Button.x = 100;
	bullet1Button.y = 330;
	bullet1Button.width = 100;
	bullet1Button.height = 40;
	bullet1Button.onHover = function(){
		if(gameInfo.bullet1 <=90){
			this.color = "#E0D54B"
			this.textSize = 15
			this.text = "500$"
		} else {
			this.color = "#CC3232"
			this.textSize = 11
			this.text = "Max\nUpgrade"
		}
	}
	bullet1Button.onOutside = function(){
		this.color = "#7EB279"
		this.textSize = 11;
		this.text="Damage\nUpgrade";
	}
	bullet1Button.onPress = function(){
		if(gameInfo.bullet1 <=90){
			this.color = "#D1C343"
			this.textSize = 11
			this.text = "500$"
		} else {
			this.color = "#CC3232"
			this.textSize = 11
			this.text = "Max\nUpgrade"
		}
	}
	bullet1Button.onRelease = function(){
		if(gameInfo.money >= 500 && gameInfo.bullet1 <=90){
			gameInfo.money = gameInfo.money - 500;
			gameInfo.bullet1 = gameInfo.bullet1 +20;
			ws.send(JSON.stringify(gameInfo));
		}
	}

	bullet2Button = new Clickable();
	bullet2Button.cornerRadius = 0;
	bullet2Button.strokeWeight = 2;
	bullet2Button.textFont = fontStyle;
	bullet2Button.x = 250;
	bullet2Button.y = 330;
	bullet2Button.width = 100;
	bullet2Button.height = 40;
	bullet2Button.onHover = function(){
		this.color = "#E0D54B"
		this.textSize = 15
		this.text = "500$"
	}
	bullet2Button.onOutside = function(){
		this.color = "#7EB279"
		this.textSize = 11;
		this.text="Buy\nDynamite";
	}
	bullet2Button.onPress = function(){
		this.color = "#D1C343"
		this.textSize = 11
		this.text = "500$"
	}
	bullet2Button.onRelease = function(){
		if(gameInfo.money >= 500 ){
			gameInfo.money = gameInfo.money - 500;
			gameInfo.bullet2 ++;
			ws.send(JSON.stringify(gameInfo));
		}
	}

	bullet3Button = new Clickable();
	bullet3Button.cornerRadius = 0;
	bullet3Button.strokeWeight = 2;
	bullet3Button.textFont = fontStyle;
	bullet3Button.x = 400;
	bullet3Button.y = 330;
	bullet3Button.width = 100;
	bullet3Button.height = 40;
	bullet3Button.onHover = function(){
		this.color = "#E0D54B"
		this.textSize = 15
		this.text = "500$"
	}
	bullet3Button.onOutside = function(){
		this.color = "#7EB279"
		this.textSize = 11;
		this.text="Buy\nShark";
	}
	bullet3Button.onPress = function(){
		this.color = "#D1C343"
		this.textSize = 11
		this.text = "500$"
	}
	bullet3Button.onRelease = function(){
		if(gameInfo.money >= 500 ){
			gameInfo.money = gameInfo.money - 500;
			gameInfo.bullet3 ++;
			ws.send(JSON.stringify(gameInfo));
		}
	}

	leaderboardButton = new Clickable();
	leaderboardButton.cornerRadius = 0;
	leaderboardButton.strokeWeight = 2;
	leaderboardButton.textFont = fontStyle;
	leaderboardButton.x = 450;
	leaderboardButton.y = 10;
	leaderboardButton.width = 110;
	leaderboardButton.height = 40;
	leaderboardButton.onHover = function(){

		this.color = "#E0D54B"
	
	}
	leaderboardButton.onOutside = function(){
		this.color = "#FCEC56"
		this.textSize = 12
		this.text="Leaderboard";
	}
	leaderboardButton.onPress = function(){
	
		this.color = "#E0D54B"
		this.textSize = 11
		this.text = "Leaderboard"
		score1 =1;
		score2 =1;
		score3 =1;

		//sort "myleaderboard" based on score, total shots, and round
		if(isleaderBoardOn == 0) {
			isleaderBoardOn = 1;
			leaderBoard.sort(function(a,b){
				return a.score>b.score ? -1 : a.score<b.score? 1:0;
			});
		}
		else if(isleaderBoardOn == 1) {
			isleaderBoardOn =2;
			leaderBoard.sort(function(a,b){
				return a.totalShots>b.totalShots ? -1 : a.totalShots<b.totalShots? 1:0;
			});
		}
		else if(isleaderBoardOn == 2) {
			isleaderBoardOn =3;
			leaderBoard.sort(function(a,b){
				return a.round>b.round ? -1 : a.round<b.round? 1:0;
			});
		}
		else isleaderBoardOn = 0;
		console.log(isleaderBoardOn)
	}
	leaderboardButton.onRelease = function(){
	
		this.color = "#E0D54B"
		this.textSize = 12
		this.text = "Leaderboard"
	}


	//sprite setting
	for (let i = 0; i < 8; i++) {
	  let posX = i*100;
	  let img = spritesheet.get(posX, 0, 100,100);
	  playeranimation.push(img);
	}
	for(let i=0; i<10; i++){
		let posX = i*50;
		let img = speedspritesheet.get(posX,0,50,50);
		speedanimation.push(img);
	}
	speed = new Sprite(speedanimation, 225,130,3);
	player = new Sprite(playeranimation, 100, 80, 0.05);

}

class Sprite {
	constructor(animation, x, y, speed) {
		this.x = x;
		this.y = y;
		this.animation = animation;
		this.w = this.animation[0].width;
		this.len = this.animation.length;
		this.speed = speed;
		this.index = 0;
	}
	show() {
		let index = floor(this.index) % this.len;
		image(this.animation[index], this.x, this.y);
	}
	rotate(){
		this.index +=this.speed;
	}
	animate(){
		this.index +=this.speed/10;
		this.x += this.speed/3*2;
		if(this.x > 400) this.x = 225;
	}
}

function draw()
{

	textFont(fontStyle);
	background(221,217,199);
	leaderboardButton.draw();
	textSize(20);
	fill(200,160,0);
	text(""+gameInfo.money,110,25);
	image(moneyIcon,0,-20);

	//pop up when round is over
	if(gameInfo.gameState == 0 && isleaderBoardOn ==false )
	{
		fill(201,195,171);
		noStroke()
		rect(25,220,550,160);
		rect(25, 60,550,150);
		fill(0)
		rect(98,228,104,104);
		rect(248,228,104,104);
		rect(398,228,104,104);
		image(bullet1,100,230);
		image(bullet2,250,230);
		image(bullet3,400,230);
		fill(255,233,222);
		noStroke();
		rect(90,70,120,120);

		fill(0);
		noStroke();
		rect(230,90,200,30);
		fill(255,255,0);
		noStroke();
		rect(233,93,194/200*gameInfo.playerHealth,24);
		textSize(20);
		fill(0);
		text(""+gameInfo.bullet1,180,305);
		text(""+gameInfo.bullet2,330,305);
		text(""+gameInfo.bullet3,480,305);
		

		player.show();
		player.rotate();
		speed.show();
		speed.animate();

		healthButton.draw();
		speedButton.draw();
		bullet1Button.draw();
		bullet2Button.draw();
		bullet3Button.draw();
	}

	//pop up when click the leaderboard button
	if(isleaderBoardOn){
		noStroke();
		fill(240);
		rect(25,60,550,330);
		//top3 score
		if(isleaderBoardOn == 1){
			if(score1 <=leaderBoard[0].score/leaderBoard[0].score*150) score1 +=2;
			if(score2 <=leaderBoard[1].score/leaderBoard[0].score*150) score2 +=2;
			if(score3 <=leaderBoard[2].score/leaderBoard[0].score*150) score3 +=2;
	
			stroke(50,50,50);
			fill(35,100,200);
			scribble.scribbleEllipse(150,220,score1,score1);
			fill(70,140,220);
			scribble.scribbleEllipse(330,220,score2,score2);
			fill(100,180,240);
			scribble.scribbleEllipse(490,220,score3,score3);
			textSize(20);
			fill(20,20,20);
			noStroke();
			text(leaderBoard[0].score,150,215);
			text(leaderBoard[1].score,330,215);
			text(leaderBoard[2].score,490,215);
	
			text(leaderBoard[0].name,150,320);
			text(leaderBoard[1].name,330,320);
			text(leaderBoard[2].name,490,320);
	
			text("Best Score",300,100);
		}
		//top3 totalshots
		if(isleaderBoardOn == 2){
			if(score1 <=leaderBoard[0].totalShots/leaderBoard[0].totalShots*150) score1 +=2;
			if(score2 <=leaderBoard[1].totalShots/leaderBoard[0].totalShots*150) score2 +=2;
			if(score3 <=leaderBoard[2].totalShots/leaderBoard[0].totalShots*150) score3+=2;
	
			stroke(50,50,50);
			fill(200,35,100);
			scribble.scribbleEllipse(150,220,score1,score1);
			fill(220,70,140);
			scribble.scribbleEllipse(330,220,score2,score2);
			fill(240,100,180);
			scribble.scribbleEllipse(490,220,score3,score3);
			textSize(20);
			fill(20,20,20);
			noStroke();
			text(leaderBoard[0].totalShots,150,215);
			text(leaderBoard[1].totalShots,330,215);
			text(leaderBoard[2].totalShots,490,215);
	
			text(leaderBoard[0].name,150,320);
			text(leaderBoard[1].name,330,320);
			text(leaderBoard[2].name,490,320);
	
			text("Total Bullet Used",300,100);
		}
		//top3 round reached
		if(isleaderBoardOn == 3){
			if(score1 <=leaderBoard[0].round/leaderBoard[0].round*150) score1 +=2;
			if(score2 <=leaderBoard[1].round/leaderBoard[0].round*150) score2 +=2;
			if(score3 <=leaderBoard[2].round/leaderBoard[0].round*150) score3 +=2;
	
			stroke(50,50,50);
			fill(100,200,35);
			scribble.scribbleEllipse(150,220,score1,score1);
			fill(140,220,70);
			scribble.scribbleEllipse(330,220,score2,score2);
			fill(180,240,100);
			scribble.scribbleEllipse(490,220,score3,score3);
			textSize(20);
			fill(20,20,20);
			noStroke();
			text(leaderBoard[0].round,150,215);
			text(leaderBoard[1].round,330,215);
			text(leaderBoard[2].round,490,215);
	
			text(leaderBoard[0].name,150,320);
			text(leaderBoard[1].name,330,320);
			text(leaderBoard[2].name,490,320);
	
			text("Best Score",300,100);
		}
	}
	if(gameInfo.gameState == 1 || gameInfo.gameState == 2)
	{
		fill(100)
		rect(0,0,width,height);
		textSize(30);
		fill(255);
		text("Game In Progress",300,200);
	}
}

//load "myLeaderboard.json" file
var json = $.getJSON("myLeaderboard.json", function(json) {
	leaderBoard = json.stats;
	leaderBoard.sort(function(a,b){
		return a.score>b.score ? -1 : a.score<b.score? 1:0;
	});
});

// called when loading the page
$(function () {
ws = new WebSocket("ws://localhost:8025/test");

ws.onopen = function () {
	// Web Socket is connected, send data using send()
	console.log("Ready...");
};

ws.onmessage = function (evt) {
	var received_msg = evt.data;
	gameInfo = JSON.parse(received_msg);
	console.log("Message is received...");
	console.log(gameInfo);
};

ws.onclose = function () {
	// websocket is closed.
	console.log("Connection is closed...");
};
});