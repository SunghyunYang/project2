/*
	Project 1
	Name of Project: Marine vs Zerg
	Author: Sunghyun Yang
	Date:2020.05.29
*/

import websockets.*;
import java.util.Iterator;

import ddf.minim.*;
Minim gallag;
Minim ak74fire;
Minim spas12reload;
Minim ruger77fire;
AudioPlayer alarm;
AudioPlayer gunshot;
AudioPlayer boom;
AudioPlayer reload;

import sprites.*;
import sprites.maths.*;
import sprites.utils.*;
StopWatch stopwatch = new StopWatch();
Sprite explosion;
float px, py;

WebsocketServer ws;

Player myPlayer;
Game game;
EnemyFactory f;
ArrayList<Bullet> bullets;
ArrayList<Enemy> enemies;
ArrayList<Item> items;
ArrayList<ArrayList> everystats;
ArrayList<String> stats;

int width = 800;
int height = 800;

PImage bg;
PImage sb;
JSONObject gameInfo;
Rounds rounds;
int enemyNumber;
String playerName;

//gameState = 0  -> waiting for the next round
//gameState = 1  -> Next round Called. Enemies creating before round starts
//gameState = 2  -> Round in progress. If it ends, gameState becomes 0
//gameState = 4  -> gameOver.

void setup()
{
gameInfo = new JSONObject();
gameInfo.setInt("playerSpeed",3);
gameInfo.setInt("playerHealth",200);
gameInfo.setInt("gameState",0);
gameInfo.setInt("round",1);
gameInfo.setInt("bullet1",40);
gameInfo.setInt("bullet2",3);
gameInfo.setInt("bullet3",3);
gameInfo.setInt("score",0);
gameInfo.setInt("money",500);
gameInfo.setInt("totalShots",0);
gameInfo.setInt("totalKills",0);

println(gameInfo);



everystats = new ArrayList<ArrayList>();
loadLeaderboard("../Javascript_Code/myLeaderboard.JSON");

//////////////////sound///////////////////
  ak74fire = new Minim(this);
  spas12reload = new Minim(this);
  ruger77fire = new Minim(this);
  gallag = new Minim(this);
  alarm =  gallag.loadFile("alarm.wav");
  gunshot = ak74fire.loadFile("ak74-fire.wav");
  reload = spas12reload.loadFile("spas12-reload.wav");
  boom = ruger77fire.loadFile("ruger77-fire.wav");


  ws = new WebsocketServer(this, 8025, "/test");

/////////////////game components/////////////
  explosion = new Sprite(this, "explosion.png", 6, 7, 1);
  size(800,800);
  bg = loadImage("background.png");
  sb = loadImage("statusbar.png");
  bullets = new ArrayList<Bullet>(); 
  enemies = new ArrayList<Enemy>();
  items = new ArrayList<Item>();
  game = new Game();
  f = new EnemyFactory();
  myPlayer = new Player(300,300,80,80,gameInfo.getInt("playerSpeed"),gameInfo.getInt("playerHealth"));
  rounds = new Rounds();
  rounds.draw();
  playerName = "";

  ws.sendMessage(gameInfo.toString());
}

void webSocketServerEvent(String msg) 
{
  JSONObject json= parseJSONObject(msg);
  try
  {
    if (json!=null)  
      {
        println("Received:");
        alarm.play(100);
        gameInfo = json;
        println(gameInfo);
        myPlayer.setSpeed(gameInfo.getInt("playerSpeed"));
        myPlayer.setHealth(gameInfo.getInt("playerHealth"));
        println("================");
      }
    
  }catch (Exception ie)
  {
    println("Exception: Invalid command");
  }
}
void draw()
{
  background(bg);
  
  rounds.draw();
  //wait for the next round
  if(gameInfo.getInt("gameState") == 0)
  {
    fill(255);
    textSize(35);
    game.hitTest();
    text("Press Space for the Round"+rounds.getRound(),100,100);
  }
  //enemies creating
  if(gameInfo.getInt("gameState") == 1)
  {
    fill(255);
    textSize(35);
    text("Round "+rounds.getRound(),100,200);
  }
  //round starts
  if(gameInfo.getInt("gameState") == 2)
  {
    game.updateAndDraw();
    game.hitTest();
    if(myPlayer.getHealth() <=0)
    {
      myPlayer.pHealth =0;
      gameInfo.setInt("gameState",4);
    }
  }
  //game ends

  game.itemDraw();
  myPlayer.updatePositionAndDraw();
  image(sb,400,700,800,200);
  game.statusBar();
  if(gameInfo.getInt("gameState") == 4)
    {
      fill(255);
      textSize(35);
      text("Game Over",100,100);
      text("Your Score : "+gameInfo.getInt("score"),100,140);
      text("Press Space to Restart",100,180);
      fill(200);
      noStroke();
      rect(200,250,400,150);
      fill(255);
      noStroke();
      rect(220,330, 300,50);
      fill(0);
      textSize(25);
      text("Enter your Name", 230,300);
      text(playerName,230,360);
      textSize(15);
      text("<Enter>",530,360);
    }

  float elapsedTime = (float) stopwatch.getElapsedTime();
  S4P.updateSprites(elapsedTime);
  S4P.drawSprites();
}
/*
void mousePressed(){
	JSONObject testjson = loadJSONObject("../Javascript_Code/myLeaderboard.JSON");
  println(testjson);
  ws.sendMessage(testjson.toString());

}
*/

void keyPressed()
{
  //press space to start next round
  if(gameInfo.getInt("gameState") == 0 )
  {
    if(key ==' ')
    {gameInfo.setInt("gameState",1);

    ws.sendMessage(gameInfo.toString());
    }
  }
  //press space to restart game
  if( gameInfo.getInt("gameState") == 4)
  {
    if(key > 64 && key < 123){
    playerName +=key;
    }
    if(key == ENTER )
    {
      stats = new ArrayList<String>();
    	stats.add(playerName);
      stats.add(""+gameInfo.getInt("score"));
    	stats.add(""+gameInfo.getInt("round"));
      stats.add(""+gameInfo.getInt("totalShots"));
      everystats.add(stats);
      saveLeaderboard("../Javascript_Code/myLeaderboard.JSON");
      playerName ="";
      myPlayer.setHealth(200);
      rounds = new Rounds();
      items = new ArrayList<Item>();
      gameInfo.setInt("playerSpeed",3);
      gameInfo.setInt("playerHealth",200);
      gameInfo.setInt("gameState",0);
      gameInfo.setInt("round",1);
      gameInfo.setInt("bullet1",40);
      gameInfo.setInt("bullet2",3);
      gameInfo.setInt("bullet3",3);
      gameInfo.setInt("score",0);
      gameInfo.setInt("money",500);
      gameInfo.setInt("totalShots",0);
      gameInfo.setInt("totalKills",0);

      ws.sendMessage(gameInfo.toString());
      rounds.round = 1;
    }
    if(key == BACKSPACE)
    {
      if(playerName.length() >= 1)
      playerName = playerName.substring(0,playerName.length()-1);
    }
  }
  //press z,x,c to shoot bullet
  if(gameInfo.getInt("gameState") == 2)
  {
    if(key == 'z')
    {
      gameInfo.setInt("totalShots",gameInfo.getInt("totalShots")+1);
      gunshot.play(100);
      game.shootBullet(1,myPlayer.getDirection(),myPlayer.getX(),myPlayer.getY(),10,25);
    }
    if(key == 'x')
    {
      if(gameInfo.getInt("bullet2")>0)
      {
        gameInfo.setInt("totalShots",gameInfo.getInt("totalShots")+1);
        boom.play(100);
        game.shootBullet(2,myPlayer.getDirection(),myPlayer.getX(),myPlayer.getY(),20,50);
        gameInfo.setInt("bullet2",gameInfo.getInt("bullet2") -1);
      }
    }  
    if(key == 'c')
    {
      if(gameInfo.getInt("bullet3")>0)
      {
        gameInfo.setInt("totalShots",gameInfo.getInt("totalShots")+1);
        boom.play(100);
        game.shootBullet(3,myPlayer.getDirection(),myPlayer.getX(),myPlayer.getY(),45,75);
        gameInfo.setInt("bullet3",gameInfo.getInt("bullet3") -1);
      }
    } 
  }
  if(key ==CODED)
  {
    if(keyCode == LEFT)
    {
      myPlayer.pLeft = true;
    }
    else if(keyCode == RIGHT)
    {
      myPlayer.pRight = true;
    }
    else if(keyCode == UP)
    {
      myPlayer.pUp = true;
    }
    else if(keyCode == DOWN)
    {
      myPlayer.pDown = true;
    }
  }
  
}

void keyReleased()
{
  if(key == CODED)
  {
    if(keyCode == LEFT)
    {
      myPlayer.pLeft = false;
    }
    if(keyCode == RIGHT)
    {
      myPlayer.pRight = false;
    }
    if(keyCode == UP)
    {
      myPlayer.pUp = false;
    }
    if(keyCode == DOWN)
    {
      myPlayer.pDown = false;
    }
  }
}



class Game 
{
  void statusBar()
  {
    fill(255);
    textSize(15);
    text("Health : "+myPlayer.getHealth(),200,700);
    text("Score : "+gameInfo.getInt("score"),200,730);
    text("Money : "+gameInfo.getInt("money"),200,760);
    fill(255);
    textSize(25);
    text(gameInfo.getInt("bullet2"),690,770);
    text(gameInfo.getInt("bullet3"),760,770);
  }
  //shoot a bullet
  void shootBullet(int bulletType,int direction, float x, float y,float w, float h)
  {
      if(bulletType == 1)
      bullets.add(new Bullet1(x,y,w,h,direction,10,gameInfo.getInt("bullet1")));
      if(bulletType == 2)
      bullets.add(new Dynamite(x,y,w,h,direction,10,100));
      if(bulletType == 3)
      bullets.add(new Shark(x,y,w,h,direction,7,20));
  }
  
  //update the bullets & enemie' statement and draw
  void updateAndDraw()
  {
    for(Bullet bullet : bullets) 
    {        
      //update the bullets' movement depends on Player's direction
      //in this case, I didn't use setter method because it decreases the frame rate.
      if(bullet.getVisible())
      {
        if(bullet.getDirection()==0)
        {
        bullet.y -= bullet.getSpeed();
        }      
        else if(bullet.getDirection()==1)
        {
        bullet.y -= bullet.getSpeed()*0.7;
        bullet.x += bullet.getSpeed()*0.7;
        }
        else if(bullet.getDirection()==2)
        {
        bullet.x += bullet.getSpeed();
        }
        else if(bullet.getDirection()==3)
        {
        bullet.y += bullet.getSpeed()*0.7;
        bullet.x += bullet.getSpeed()*0.7;
        }
        else if(bullet.getDirection()==4)
        {
        bullet.y += bullet.getSpeed();
        }
        else if(bullet.getDirection()==5)
        {
        bullet.y += bullet.getSpeed()*0.7;
        bullet.x -= bullet.getSpeed()*0.7;
        }
        else if(bullet.getDirection()==6)
        {
        bullet.x -= bullet.getSpeed();
        }
        else if(bullet.getDirection()==7)
        {
        bullet.y -= bullet.getSpeed()*0.7;
        bullet.x -= bullet.getSpeed()*0.7;
        }
        bullet.bSpeed -= bullet.getDamper();
        bullet.draw();
      }

      //BulletType : 2      Dynamite's Explosiion
      if(bullet.getVisible() && bullet.getSpeed() <0 && bullet.getType() == 2)
      {
        boom.play(100);
        bullets.add(new Explosion(bullet.getX(),bullet.getY(),130,90,8,0,5));
        bullet.setVisible(false);
        return;
      }

      //out of map boundary
      if(bullet.getX()<0 ||bullet.getX()>width||bullet.getY()<0|| bullet.getY()>height)
      {
        bullet.setVisible(false);
      }
    }
    for(Enemy enemy : enemies) 
    {
      //EnemyType : 2        mutallisk shoot Bug when player approaches
      if(enemy.getType() ==2 && enemy.getVisible())
      {
        if(abs(enemy.getX()-myPlayer.getX())+abs(enemy.getY()-myPlayer.getY())<300)
        {
          enemies.add(new Bug(enemy.getX(),enemy.getY(),40,40,2,10,100));
          enemy.setType(4);
          enemyNumber +=1;
          return;
        }
      }
      
      //update the enemies' movement
      //Also in this case, I didn't use setter method because it decreases the frame rate.
      if(enemy.getVisible())
      {
        enemy.setDirection(enemy.getX()-myPlayer.getX(),enemy.getY()-myPlayer.getY());
        enemy.x -=enemy.getSpeed()*enemy.getDirection().x;
        enemy.y -=enemy.getSpeed()*enemy.getDirection().y;
        enemy.draw();
      }
    }
  }

  //items draw separately with bullets&enemies, because items remains on the floor until next round starts
  void itemDraw()
  {
    for(Item item : items)
    {
      if(item.getVisible())item.draw();
    }
  }

  //collision check among enemies, player, bullets, items
  void hitTest()
  {
    // hitTest with enemies <-> Player
    for (Enemy enemy : enemies)
    {
      if(enemy.getVisible())
      {
        if(enemy.getX()+enemy.getW()/2 > myPlayer.getX()-myPlayer.getW()/2+15 && enemy.getX()-enemy.getW()/2 < myPlayer.getX()+myPlayer.getW()/2-15)
        {
          if(enemy.getY()+enemy.getH()/2 > myPlayer.getY()-myPlayer.getH()/2+15 && enemy.getY()-enemy.getH()/2 < myPlayer.getY()+myPlayer.getH()/2-15)
          {
            myPlayer.pHealth -= enemy.getDamage();
            boom.play(100);
            enemyNumber -=1;
            enemy.setVisible(false);
            explosion.setXY(myPlayer.getX(), myPlayer.getY());
            explosion.setFrameSequence(0, 42, 0.02, 1);
          }
        }
      }
    }

    // hitTest with enemies <-> bullets
    for(Bullet bullet : bullets)
    {
      if(bullet.getVisible())
      {
        for(Enemy enemy : enemies)
        {
          if(enemy.getVisible())
          {
            if(abs(enemy.getX()-bullet.getX())<enemy.getW()/2+bullet.getW()/2 && abs(enemy.getY()-bullet.getY())<enemy.getH()/2+bullet.getH()/2)
            {
              if(bullet.getType() == 1)
              {
                enemy.eHealth -= bullet.getDamage();
                bullet.setVisible(false);
              }
              if(bullet.getType() == 2)
              {
                enemy.eHealth -= bullet.getDamage();
                bullet.setVisible(false);
                boom.play(100);
                bullets.add(new Explosion(bullet.getX(),bullet.getY(),130,90,8,0,5));
                return;
              }
              if(bullet.getType() == 3)
              {
                enemy.eHealth -= bullet.getDamage();
                bullet.count -= 1;
                if(bullet.count <=0) bullet.setVisible(false);
              }
              if(bullet.getType()==4)
              {
                enemy.eHealth -= bullet.getDamage();
              }
              
              //enemies died.
              if(enemy.getHealth()<=0)
              {
                int x;
                x = int(random(0,10));
                enemy.setVisible(false);enemyNumber -=1;
                gameInfo.setInt("score",gameInfo.getInt("score")+ 100);
                gameInfo.setInt("money",gameInfo.getInt("money")+ 40);
                gameInfo.setInt("totalKills",gameInfo.getInt("totalKills")+1);
                if(x ==1 )
                  items.add(new ItemDynamite(enemy.getX(),enemy.getY(),20,50));
                else if( x== 2)
                  items.add(new ItemShark(enemy.getX(),enemy.getY(),30,50));
              }
            }
          }
        }
      }
    }

    // hitTest with items <-> myPlayer
    for(Item item : items)
    {
      if(item.getVisible())
      {
        if(item.getX()+item.getW()/2 > myPlayer.getX()-myPlayer.getW()/2+15 && item.getX()-item.getW()/2 < myPlayer.getX()+myPlayer.getW()/2-15)
        {
          if(item.getY()+item.getH()/2 > myPlayer.getY()-myPlayer.getH()/2+15 && item.getY()-item.getH()/2 < myPlayer.getY()+myPlayer.getH()/2-15)
          {
            reload.play(100);
            item.setVisible(false);
            if(item.getType() ==2) gameInfo.setInt("bullet2",gameInfo.getInt("bullet2") +1);;
            if(item.getType() ==3) gameInfo.setInt("bullet3",gameInfo.getInt("bullet3") +1);;

            ws.sendMessage(gameInfo.toString());
          }
        }
      }
    }
  }
}


class Rounds extends Thread
{
  int round = 1;
  Rounds(){start();}
  int getRound(){return round;}
  void draw(){
  }
  void run() {
    while(true)
    {
      //wait until round Starts
      while(gameInfo.getInt("gameState") == 0)
      {
        try {
          Thread.sleep (100);
        } catch (Exception e) {}
      }

      //create enemies before round Starts
      if(gameInfo.getInt("gameState") ==1)
      {
        bullets = new ArrayList<Bullet>(); 
        enemies = new ArrayList<Enemy>();
        items = new ArrayList<Item>();

        for(int i = 0; i<round; i++)
        {
          enemies.add(f.createZergling(0-150*i,int(random(0, 600))));
          enemies.add(f.createZergling(800+150*i,int(random(0,600))));
          if(i>1)
          {
            enemies.add(f.createZergling(int(random(0, 800)),0-150*(i-2)));
            enemies.add(f.createZergling(int(random(0, 800)),700+150*(i-2)));
          }
          if(i>3)
          {
            enemies.add(f.createMutallisk(0-150*(i-2),int(random(0, 600))));
            enemies.add(f.createMutallisk(800+150*(i-2),int(random(0, 600))));
          }
          if(i>5)
          {
            enemies.add(f.createMutallisk(int(random(0, 800)),0-150*(i-4)));
            enemies.add(f.createMutallisk(int(random(0, 800)),700+150*(i-4)));
          }
        }
        try {
          Thread.sleep (2000);
        } catch (Exception e) {}

        gameInfo.setInt("gameState",2);
      }

      //wait until round Ends
      while(gameInfo.getInt("gameState") == 2)
      {
        try {
          Thread.sleep (100);
        } catch (Exception e) {}
        if(enemyNumber <=0)
        {
          gameInfo.setInt("gameState",0); 
          gameInfo.setInt("playerHealth",myPlayer.getHealth());
          ws.sendMessage(gameInfo.toString());
          round ++;
          gameInfo.setInt("round",gameInfo.getInt("round")+1);
        }
      }
    }
  }
}

void loadLeaderboard (String filename)
{
	// If file does not exists
	File f = new File(sketchPath(filename));
	if(!f.exists()) return;

	JSONObject json = loadJSONObject(filename);
	JSONArray sts = json.getJSONArray("stats");
	for (int i = 0; i < sts.size(); i++) 
	{

      stats = new ArrayList<String>();
    	JSONObject stat= sts.getJSONObject(i); 
    	stats.add(stat.getString("name"));
      stats.add(""+stat.getInt("score"));
      stats.add(""+stat.getInt("round"));
      stats.add(""+stat.getInt("totalShots"));
      everystats.add(stats);
    }
	
}

void saveLeaderboard (String filename)
{
	JSONObject json = new JSONObject();
	JSONArray sts = new JSONArray();

	json.setJSONArray("stats", sts);
	int index=0;
	for (ArrayList s: everystats)
	{
		JSONObject stat = new JSONObject();
		stat.setString("name", s.get(0).toString());
		stat.setInt("score",parseInt(s.get(1).toString()));
    stat.setInt("round",parseInt(s.get(2).toString()));
    stat.setInt("totalShots",parseInt(s.get(3).toString()));
		sts.setJSONObject(index, stat); 
		index++;
	}

	saveJSONObject(json, filename);
}
