class Player
{
  float pSpeed;
  PImage[] pImgs;
  int pDirection;
  int pHealth;
  float x,y,w,h;
  boolean pLeft,pRight,pUp,pDown = false;
 
  Player(float x,float y, float width, float height, int speed, int health)
  {
    pImgs = new PImage [8];
    pSpeed = speed;
    pHealth = health;
    for(int i=0; i<8; i++)
    {
    pImgs[i] = loadImage("player"+i+".png");
    }
    w = width;
    h= height;
    pDirection = 2;
    
    this.x = x;
    this.y = y;
  }

  float getX(){return x;}
  float getY(){return y;}
  float getW(){return w;}
  float getH(){return h;}
  int getHealth(){return pHealth;}
  int getDirection(){return pDirection;}
  void setSpeed(int a){pSpeed = a;}
  void setHealth(int a){pHealth = a;}
  void setDirection()
  {
    if(pLeft)
    {
      if(pUp) pDirection = 7;
      else if(pDown) pDirection = 5;
      else pDirection = 6;
    }
    else if(pRight)
    {
      if(pUp) pDirection = 1;
      else if(pDown) pDirection = 3;
      else pDirection = 2;
    }
    else if(pUp) pDirection = 0;
    else if(pDown) pDirection = 4;
  }

  void updatePositionAndDraw()
  {
    move();
    setDirection();
    imageMode(CENTER);
    image(pImgs[pDirection],x,y,w,h);
    fill(0);
    rectMode(CORNER);
    rect(x-25,y+40,50,10);
    if(pHealth <100)
    fill(255,0,0);
    else fill(255,255,0);
    rectMode(CORNER);
    rect(x-24,y+41,48*(float(pHealth)/200),8);
  }
  void move()
  {
    if(pLeft)
      if(myPlayer.getX()>30) x -= pSpeed;
    if(pRight)
      if(myPlayer.getX()<770) x += pSpeed;
    if(pUp)
      if(myPlayer.getY()>40) y -= pSpeed;
    if(pDown)
      if(myPlayer.getY()<610) y += pSpeed;
  }

}
