abstract class Item
{
    protected float x,y,w,h;
    protected boolean iVisible;
    protected PImage iImg;
    protected int iType;

    Item(float x, float y, float w, float h)
    {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        iVisible = true;
    }
    void draw()
    {
        if(iVisible)
        {
            pushMatrix();
            translate(x,y);
            imageMode(CENTER);
            rotate(radians(45));
            image(iImg,0,0,w,h);
            popMatrix();
        }
    }
    void setVisible(boolean a)
    {
        iVisible = a;
    }
    boolean getVisible(){return iVisible;}
    float getX(){return x;}
    float getY(){return y;}
    float getW(){return w;}
    float getH(){return h;}
    int getType(){return iType;}
}

class ItemDynamite extends Item
{
    ItemDynamite(float x, float y, float w, float h)
    {
        super(x,y,w,h);
        iImg = loadImage("bullet2.png");
        iType = 2;
    }
}
class ItemShark extends Item
{
    ItemShark(float x, float y, float w, float h)
    {
        super(x,y,w,h);
        iImg = loadImage("bullet4.png");
        iType = 3;
    }
    void draw()
    {
        if(iVisible)
        {
            pushMatrix();
            translate(x,y);
            imageMode(CENTER);
            rotate(radians(225));
            image(iImg,0,0,w,h);
            popMatrix();
        }
    }
}