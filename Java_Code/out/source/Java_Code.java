import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import websockets.*; 
import java.util.Iterator; 
import ddf.minim.*; 
import sprites.*; 
import sprites.maths.*; 
import sprites.utils.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Java_Code extends PApplet {

/*
	Project 1
	Name of Project: Marine vs Zerg
	Author: Sunghyun Yang
	Date:2020.05.29
*/





Minim gallag;
Minim ak74fire;
Minim spas12reload;
Minim ruger77fire;
AudioPlayer alarm;
AudioPlayer gunshot;
AudioPlayer boom;
AudioPlayer reload;




StopWatch stopwatch = new StopWatch();
Sprite explosion;
float px, py;

WebsocketServer ws;

Player myPlayer;
Game game;
EnemyFactory f;
ArrayList<Bullet> bullets;
ArrayList<Enemy> enemies;
ArrayList<Item> items;
ArrayList<ArrayList> everystats;
ArrayList<String> stats;

int width = 800;
int height = 800;

PImage bg;
PImage sb;
JSONObject gameInfo;
Rounds rounds;
int enemyNumber;
String playerName;

//gameState = 0  -> waiting for the next round
//gameState = 1  -> Next round Called. Enemies creating before round starts
//gameState = 2  -> Round in progress. If it ends, gameState becomes 0
//gameState = 4  -> gameOver.

public void setup()
{
gameInfo = new JSONObject();
gameInfo.setInt("playerSpeed",3);
gameInfo.setInt("playerHealth",200);
gameInfo.setInt("gameState",0);
gameInfo.setInt("round",1);
gameInfo.setInt("bullet1",40);
gameInfo.setInt("bullet2",3);
gameInfo.setInt("bullet3",3);
gameInfo.setInt("score",0);
gameInfo.setInt("money",500);
gameInfo.setInt("totalShots",0);
gameInfo.setInt("totalKills",0);

println(gameInfo);



everystats = new ArrayList<ArrayList>();
loadLeaderboard("../Javascript_Code/myLeaderboard.JSON");

//////////////////sound///////////////////
  ak74fire = new Minim(this);
  spas12reload = new Minim(this);
  ruger77fire = new Minim(this);
  gallag = new Minim(this);
  alarm =  gallag.loadFile("alarm.wav");
  gunshot = ak74fire.loadFile("ak74-fire.wav");
  reload = spas12reload.loadFile("spas12-reload.wav");
  boom = ruger77fire.loadFile("ruger77-fire.wav");


  ws = new WebsocketServer(this, 8025, "/test");

/////////////////game components/////////////
  explosion = new Sprite(this, "explosion.png", 6, 7, 1);
  
  bg = loadImage("background.png");
  sb = loadImage("statusbar.png");
  bullets = new ArrayList<Bullet>(); 
  enemies = new ArrayList<Enemy>();
  items = new ArrayList<Item>();
  game = new Game();
  f = new EnemyFactory();
  myPlayer = new Player(300,300,80,80,gameInfo.getInt("playerSpeed"),gameInfo.getInt("playerHealth"));
  rounds = new Rounds();
  rounds.draw();
  playerName = "";

  ws.sendMessage(gameInfo.toString());
}

public void webSocketServerEvent(String msg) 
{
  JSONObject json= parseJSONObject(msg);
  try
  {
    if (json!=null)  
      {
        println("Received:");
        alarm.play(100);
        gameInfo = json;
        println(gameInfo);
        myPlayer.setSpeed(gameInfo.getInt("playerSpeed"));
        myPlayer.setHealth(gameInfo.getInt("playerHealth"));
        println("================");
      }
    
  }catch (Exception ie)
  {
    println("Exception: Invalid command");
  }
}
public void draw()
{
  background(bg);
  
  rounds.draw();
  //wait for the next round
  if(gameInfo.getInt("gameState") == 0)
  {
    fill(255);
    textSize(35);
    game.hitTest();
    text("Press Space for the Round"+rounds.getRound(),100,100);
  }
  //enemies creating
  if(gameInfo.getInt("gameState") == 1)
  {
    fill(255);
    textSize(35);
    text("Round "+rounds.getRound(),100,200);
  }
  //round starts
  if(gameInfo.getInt("gameState") == 2)
  {
    game.updateAndDraw();
    game.hitTest();
    if(myPlayer.getHealth() <=0)
    {
      myPlayer.pHealth =0;
      gameInfo.setInt("gameState",4);
    }
  }
  //game ends

  game.itemDraw();
  myPlayer.updatePositionAndDraw();
  image(sb,400,700,800,200);
  game.statusBar();
  if(gameInfo.getInt("gameState") == 4)
    {
      fill(255);
      textSize(35);
      text("Game Over",100,100);
      text("Your Score : "+gameInfo.getInt("score"),100,140);
      text("Press Space to Restart",100,180);
      fill(200);
      noStroke();
      rect(200,250,400,150);
      fill(255);
      noStroke();
      rect(220,330, 300,50);
      fill(0);
      textSize(25);
      text("Enter your Name", 230,300);
      text(playerName,230,360);
      textSize(15);
      text("<Enter>",530,360);
    }

  float elapsedTime = (float) stopwatch.getElapsedTime();
  S4P.updateSprites(elapsedTime);
  S4P.drawSprites();
}
/*
void mousePressed(){
	JSONObject testjson = loadJSONObject("../Javascript_Code/myLeaderboard.JSON");
  println(testjson);
  ws.sendMessage(testjson.toString());

}
*/

public void keyPressed()
{
  //press space to start next round
  if(gameInfo.getInt("gameState") == 0 )
  {
    if(key ==' ')
    {gameInfo.setInt("gameState",1);

    ws.sendMessage(gameInfo.toString());
    }
  }
  //press space to restart game
  if( gameInfo.getInt("gameState") == 4)
  {
    if(key > 64 && key < 123){
    playerName +=key;
    }
    if(key == ENTER )
    {
      stats = new ArrayList<String>();
    	stats.add(playerName);
      stats.add(""+gameInfo.getInt("score"));
    	stats.add(""+gameInfo.getInt("round"));
      stats.add(""+gameInfo.getInt("totalShots"));
      everystats.add(stats);
      saveLeaderboard("../Javascript_Code/myLeaderboard.JSON");
      playerName ="";
      myPlayer.setHealth(200);
      rounds = new Rounds();
      items = new ArrayList<Item>();
      gameInfo.setInt("playerSpeed",3);
      gameInfo.setInt("playerHealth",200);
      gameInfo.setInt("gameState",0);
      gameInfo.setInt("round",1);
      gameInfo.setInt("bullet1",40);
      gameInfo.setInt("bullet2",3);
      gameInfo.setInt("bullet3",3);
      gameInfo.setInt("score",0);
      gameInfo.setInt("money",500);
      gameInfo.setInt("totalShots",0);
      gameInfo.setInt("totalKills",0);

      ws.sendMessage(gameInfo.toString());
      rounds.round = 1;
    }
    if(key == BACKSPACE)
    {
      if(playerName.length() >= 1)
      playerName = playerName.substring(0,playerName.length()-1);
    }
  }
  //press z,x,c to shoot bullet
  if(gameInfo.getInt("gameState") == 2)
  {
    if(key == 'z')
    {
      gameInfo.setInt("totalShots",gameInfo.getInt("totalShots")+1);
      gunshot.play(100);
      game.shootBullet(1,myPlayer.getDirection(),myPlayer.getX(),myPlayer.getY(),10,25);
    }
    if(key == 'x')
    {
      if(gameInfo.getInt("bullet2")>0)
      {
        gameInfo.setInt("totalShots",gameInfo.getInt("totalShots")+1);
        boom.play(100);
        game.shootBullet(2,myPlayer.getDirection(),myPlayer.getX(),myPlayer.getY(),20,50);
        gameInfo.setInt("bullet2",gameInfo.getInt("bullet2") -1);
      }
    }  
    if(key == 'c')
    {
      if(gameInfo.getInt("bullet3")>0)
      {
        gameInfo.setInt("totalShots",gameInfo.getInt("totalShots")+1);
        boom.play(100);
        game.shootBullet(3,myPlayer.getDirection(),myPlayer.getX(),myPlayer.getY(),45,75);
        gameInfo.setInt("bullet3",gameInfo.getInt("bullet3") -1);
      }
    } 
  }
  if(key ==CODED)
  {
    if(keyCode == LEFT)
    {
      myPlayer.pLeft = true;
    }
    else if(keyCode == RIGHT)
    {
      myPlayer.pRight = true;
    }
    else if(keyCode == UP)
    {
      myPlayer.pUp = true;
    }
    else if(keyCode == DOWN)
    {
      myPlayer.pDown = true;
    }
  }
  
}

public void keyReleased()
{
  if(key == CODED)
  {
    if(keyCode == LEFT)
    {
      myPlayer.pLeft = false;
    }
    if(keyCode == RIGHT)
    {
      myPlayer.pRight = false;
    }
    if(keyCode == UP)
    {
      myPlayer.pUp = false;
    }
    if(keyCode == DOWN)
    {
      myPlayer.pDown = false;
    }
  }
}



class Game 
{
  public void statusBar()
  {
    fill(255);
    textSize(15);
    text("Health : "+myPlayer.getHealth(),200,700);
    text("Score : "+gameInfo.getInt("score"),200,730);
    text("Money : "+gameInfo.getInt("money"),200,760);
    fill(255);
    textSize(25);
    text(gameInfo.getInt("bullet2"),690,770);
    text(gameInfo.getInt("bullet3"),760,770);
  }
  //shoot a bullet
  public void shootBullet(int bulletType,int direction, float x, float y,float w, float h)
  {
      if(bulletType == 1)
      bullets.add(new Bullet1(x,y,w,h,direction,10,gameInfo.getInt("bullet1")));
      if(bulletType == 2)
      bullets.add(new Dynamite(x,y,w,h,direction,10,100));
      if(bulletType == 3)
      bullets.add(new Shark(x,y,w,h,direction,7,20));
  }
  
  //update the bullets & enemie' statement and draw
  public void updateAndDraw()
  {
    for(Bullet bullet : bullets) 
    {        
      //update the bullets' movement depends on Player's direction
      //in this case, I didn't use setter method because it decreases the frame rate.
      if(bullet.getVisible())
      {
        if(bullet.getDirection()==0)
        {
        bullet.y -= bullet.getSpeed();
        }      
        else if(bullet.getDirection()==1)
        {
        bullet.y -= bullet.getSpeed()*0.7f;
        bullet.x += bullet.getSpeed()*0.7f;
        }
        else if(bullet.getDirection()==2)
        {
        bullet.x += bullet.getSpeed();
        }
        else if(bullet.getDirection()==3)
        {
        bullet.y += bullet.getSpeed()*0.7f;
        bullet.x += bullet.getSpeed()*0.7f;
        }
        else if(bullet.getDirection()==4)
        {
        bullet.y += bullet.getSpeed();
        }
        else if(bullet.getDirection()==5)
        {
        bullet.y += bullet.getSpeed()*0.7f;
        bullet.x -= bullet.getSpeed()*0.7f;
        }
        else if(bullet.getDirection()==6)
        {
        bullet.x -= bullet.getSpeed();
        }
        else if(bullet.getDirection()==7)
        {
        bullet.y -= bullet.getSpeed()*0.7f;
        bullet.x -= bullet.getSpeed()*0.7f;
        }
        bullet.bSpeed -= bullet.getDamper();
        bullet.draw();
      }

      //BulletType : 2      Dynamite's Explosiion
      if(bullet.getVisible() && bullet.getSpeed() <0 && bullet.getType() == 2)
      {
        boom.play(100);
        bullets.add(new Explosion(bullet.getX(),bullet.getY(),130,90,8,0,5));
        bullet.setVisible(false);
        return;
      }

      //out of map boundary
      if(bullet.getX()<0 ||bullet.getX()>width||bullet.getY()<0|| bullet.getY()>height)
      {
        bullet.setVisible(false);
      }
    }
    for(Enemy enemy : enemies) 
    {
      //EnemyType : 2        mutallisk shoot Bug when player approaches
      if(enemy.getType() ==2 && enemy.getVisible())
      {
        if(abs(enemy.getX()-myPlayer.getX())+abs(enemy.getY()-myPlayer.getY())<300)
        {
          enemies.add(new Bug(enemy.getX(),enemy.getY(),40,40,2,10,100));
          enemy.setType(4);
          enemyNumber +=1;
          return;
        }
      }
      
      //update the enemies' movement
      //Also in this case, I didn't use setter method because it decreases the frame rate.
      if(enemy.getVisible())
      {
        enemy.setDirection(enemy.getX()-myPlayer.getX(),enemy.getY()-myPlayer.getY());
        enemy.x -=enemy.getSpeed()*enemy.getDirection().x;
        enemy.y -=enemy.getSpeed()*enemy.getDirection().y;
        enemy.draw();
      }
    }
  }

  //items draw separately with bullets&enemies, because items remains on the floor until next round starts
  public void itemDraw()
  {
    for(Item item : items)
    {
      if(item.getVisible())item.draw();
    }
  }

  //collision check among enemies, player, bullets, items
  public void hitTest()
  {
    // hitTest with enemies <-> Player
    for (Enemy enemy : enemies)
    {
      if(enemy.getVisible())
      {
        if(enemy.getX()+enemy.getW()/2 > myPlayer.getX()-myPlayer.getW()/2+15 && enemy.getX()-enemy.getW()/2 < myPlayer.getX()+myPlayer.getW()/2-15)
        {
          if(enemy.getY()+enemy.getH()/2 > myPlayer.getY()-myPlayer.getH()/2+15 && enemy.getY()-enemy.getH()/2 < myPlayer.getY()+myPlayer.getH()/2-15)
          {
            myPlayer.pHealth -= enemy.getDamage();
            boom.play(100);
            enemyNumber -=1;
            enemy.setVisible(false);
            explosion.setXY(myPlayer.getX(), myPlayer.getY());
            explosion.setFrameSequence(0, 42, 0.02f, 1);
          }
        }
      }
    }

    // hitTest with enemies <-> bullets
    for(Bullet bullet : bullets)
    {
      if(bullet.getVisible())
      {
        for(Enemy enemy : enemies)
        {
          if(enemy.getVisible())
          {
            if(abs(enemy.getX()-bullet.getX())<enemy.getW()/2+bullet.getW()/2 && abs(enemy.getY()-bullet.getY())<enemy.getH()/2+bullet.getH()/2)
            {
              if(bullet.getType() == 1)
              {
                enemy.eHealth -= bullet.getDamage();
                bullet.setVisible(false);
              }
              if(bullet.getType() == 2)
              {
                enemy.eHealth -= bullet.getDamage();
                bullet.setVisible(false);
                boom.play(100);
                bullets.add(new Explosion(bullet.getX(),bullet.getY(),130,90,8,0,5));
                return;
              }
              if(bullet.getType() == 3)
              {
                enemy.eHealth -= bullet.getDamage();
                bullet.count -= 1;
                if(bullet.count <=0) bullet.setVisible(false);
              }
              if(bullet.getType()==4)
              {
                enemy.eHealth -= bullet.getDamage();
              }
              
              //enemies died.
              if(enemy.getHealth()<=0)
              {
                int x;
                x = PApplet.parseInt(random(0,10));
                enemy.setVisible(false);enemyNumber -=1;
                gameInfo.setInt("score",gameInfo.getInt("score")+ 100);
                gameInfo.setInt("money",gameInfo.getInt("money")+ 40);
                gameInfo.setInt("totalKills",gameInfo.getInt("totalKills")+1);
                if(x ==1 )
                  items.add(new ItemDynamite(enemy.getX(),enemy.getY(),20,50));
                else if( x== 2)
                  items.add(new ItemShark(enemy.getX(),enemy.getY(),30,50));
              }
            }
          }
        }
      }
    }

    // hitTest with items <-> myPlayer
    for(Item item : items)
    {
      if(item.getVisible())
      {
        if(item.getX()+item.getW()/2 > myPlayer.getX()-myPlayer.getW()/2+15 && item.getX()-item.getW()/2 < myPlayer.getX()+myPlayer.getW()/2-15)
        {
          if(item.getY()+item.getH()/2 > myPlayer.getY()-myPlayer.getH()/2+15 && item.getY()-item.getH()/2 < myPlayer.getY()+myPlayer.getH()/2-15)
          {
            reload.play(100);
            item.setVisible(false);
            if(item.getType() ==2) gameInfo.setInt("bullet2",gameInfo.getInt("bullet2") +1);;
            if(item.getType() ==3) gameInfo.setInt("bullet3",gameInfo.getInt("bullet3") +1);;

            ws.sendMessage(gameInfo.toString());
          }
        }
      }
    }
  }
}


class Rounds extends Thread
{
  int round = 1;
  Rounds(){start();}
  public int getRound(){return round;}
  public void draw(){
  }
  public void run() {
    while(true)
    {
      //wait until round Starts
      while(gameInfo.getInt("gameState") == 0)
      {
        try {
          Thread.sleep (100);
        } catch (Exception e) {}
      }

      //create enemies before round Starts
      if(gameInfo.getInt("gameState") ==1)
      {
        bullets = new ArrayList<Bullet>(); 
        enemies = new ArrayList<Enemy>();
        items = new ArrayList<Item>();

        for(int i = 0; i<round; i++)
        {
          enemies.add(f.createZergling(0-150*i,PApplet.parseInt(random(0, 600))));
          enemies.add(f.createZergling(800+150*i,PApplet.parseInt(random(0,600))));
          if(i>1)
          {
            enemies.add(f.createZergling(PApplet.parseInt(random(0, 800)),0-150*(i-2)));
            enemies.add(f.createZergling(PApplet.parseInt(random(0, 800)),700+150*(i-2)));
          }
          if(i>3)
          {
            enemies.add(f.createMutallisk(0-150*(i-2),PApplet.parseInt(random(0, 600))));
            enemies.add(f.createMutallisk(800+150*(i-2),PApplet.parseInt(random(0, 600))));
          }
          if(i>5)
          {
            enemies.add(f.createMutallisk(PApplet.parseInt(random(0, 800)),0-150*(i-4)));
            enemies.add(f.createMutallisk(PApplet.parseInt(random(0, 800)),700+150*(i-4)));
          }
        }
        try {
          Thread.sleep (2000);
        } catch (Exception e) {}

        gameInfo.setInt("gameState",2);
      }

      //wait until round Ends
      while(gameInfo.getInt("gameState") == 2)
      {
        try {
          Thread.sleep (100);
        } catch (Exception e) {}
        if(enemyNumber <=0)
        {
          gameInfo.setInt("gameState",0); 
          gameInfo.setInt("playerHealth",myPlayer.getHealth());
          ws.sendMessage(gameInfo.toString());
          round ++;
          gameInfo.setInt("round",gameInfo.getInt("round")+1);
        }
      }
    }
  }
}

public void loadLeaderboard (String filename)
{
	// If file does not exists
	File f = new File(sketchPath(filename));
	if(!f.exists()) return;

	JSONObject json = loadJSONObject(filename);
	JSONArray sts = json.getJSONArray("stats");
	for (int i = 0; i < sts.size(); i++) 
	{

      stats = new ArrayList<String>();
    	JSONObject stat= sts.getJSONObject(i); 
    	stats.add(stat.getString("name"));
      stats.add(""+stat.getInt("score"));
      stats.add(""+stat.getInt("round"));
      stats.add(""+stat.getInt("totalShots"));
      everystats.add(stats);
    }
	
}

public void saveLeaderboard (String filename)
{
	JSONObject json = new JSONObject();
	JSONArray sts = new JSONArray();

	json.setJSONArray("stats", sts);
	int index=0;
	for (ArrayList s: everystats)
	{
		JSONObject stat = new JSONObject();
		stat.setString("name", s.get(0).toString());
		stat.setInt("score",parseInt(s.get(1).toString()));
    stat.setInt("round",parseInt(s.get(2).toString()));
    stat.setInt("totalShots",parseInt(s.get(3).toString()));
		sts.setJSONObject(index, stat); 
		index++;
	}

	saveJSONObject(json, filename);
}

abstract class Bullet
{
    protected int bDamage; 
    protected int bDirection;
    protected boolean bVisible;
    protected PImage bImg;
    protected float damper,bSpeed,x, y,w,h;
    protected int bType;
    protected int count;

    Bullet(float x, float y,float w, float h,int bDirection, float bSpeed, int bDamage){
        bVisible = true;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.bDirection = bDirection;
        this.bDamage = bDamage;
        this.bSpeed = bSpeed;
    }
    public void draw()
    {
        if(bVisible ==true)
        {
            pushMatrix();
            translate(x,y);
            imageMode(CENTER);
            rotate(radians(45*bDirection));
            image(bImg,0,0,w,h);
            popMatrix();
        }
    }
    public void setVisible(boolean tf){bVisible = tf;}
    public boolean getVisible(){return bVisible;}
    public float getSpeed(){return bSpeed;}
    public float getX(){return x;}
    public float getY(){return y;}
    public float getW(){return w;}
    public float getH(){return h;}
    public float getDamper(){return damper;}
    public int getDamage(){return bDamage;}
    public int getDirection(){return bDirection;}
    public int getType(){return bType;}
}

class Bullet1 extends Bullet
{
    Bullet1(float x, float y,float  w, float h, int bDirection, float bSpeed, int bDamage)
    {
        super(x,y,w,h,bDirection,bSpeed,bDamage);
        bImg = loadImage("bullet1.png");
        damper = 0;
        bType = 1;
    }
}

class Dynamite extends Bullet
{
    Dynamite(float x, float y,float w, float h, int bDirection, float bSpeed, int bDamage)
    {
        super(x,y,w,h,bDirection,bSpeed,bDamage);
        bImg = loadImage("bullet2.png");
        damper = 0.2f;
        bType = 2;
    }
    public void draw()
    {
        if(bVisible ==true)
        {
            pushMatrix();
            translate(x,y);
            imageMode(CENTER);
            rotate(radians(45*(bDirection+1)));
            image(bImg,0,0,w,h);
            popMatrix();
        }
        
    }    
}

class Explosion extends Bullet
{
    Explosion(float x, float y,float w, float h, int bDirection, float bSpeed, int bDamage)
    {
        super(x,y,w,h, bDirection,bSpeed,bDamage);
        bImg = loadImage("bullet3.png");
        damper = 0;
        bType = 3;
        count = 100;
    }
    public void draw()
    {
        if(bVisible == true)
        {
            pushMatrix();
            translate(x,y);
            imageMode(CENTER);
            image(bImg,0,0,w,h);
            popMatrix();
        }
	}
    
}

class Shark extends Bullet
{
    Shark(float x, float y,float w, float h, int bDirection, float bSpeed, int bDamage)
    {
        super(x,y,w,h, bDirection,bSpeed,bDamage);
        bImg = loadImage("bullet4.png");
        damper = 0;
        bType = 4;
    }
}
abstract class Enemy
{
    protected int eDamage,eHealth;
    protected boolean eVisible;
    protected PImage[] eImgs;
    protected float eSpeed,x,y,w,h;
    protected int eType;
    protected PVector eDirection;

    Enemy(float x, float y,float w, float h, float eSpeed, int eDamage,int eHealth)
    {
        eVisible = true;
        this.x = x;
        this.y = y;
        this.w= w;
        this.h = h;
        this.eDamage = eDamage;
        this.eSpeed = eSpeed;
        this.eHealth = eHealth;
    }
    public abstract void draw();
    public int getHealth(){return eHealth;}
    public float getX(){return x;}
    public float getY(){return y;}
    public float getW(){return w;}
    public float getH(){return h;}
    public int getDamage(){return eDamage;}
    public float getSpeed(){return eSpeed;}
    public boolean getVisible(){return eVisible;}
    public void setVisible(boolean tf){eVisible = tf;}
    public int getType(){return eType;}
    public void setType(int a){eType = a;}
    public void setDirection(float x,float y)
    {
        eDirection = new PVector(x,y);
        eDirection.normalize();
    }
    public PVector getDirection(){return eDirection;}
}

//Enemies are created at once when each round starts.
class EnemyFactory
{
  public Zergling createZergling(int x, int y)
  {
    Zergling e = new Zergling(x,y,50,50,1,10,100);
    enemyNumber +=1;
    return e;
  }
  public Mutallisk createMutallisk(int x, int y)
  {
    Mutallisk e = new Mutallisk(x,y,60,60,1,7,200);
    enemyNumber +=1;
    return e;
  }
}

class Zergling extends Enemy
{
    Zergling(float x, float y,float w, float h, float eSpeed, int eDamage, int eHealth)
    {
        super(x,y,w,h,eSpeed,eDamage,eHealth);
        eImgs = new PImage[2];
        eImgs[0] = loadImage("enemy1.png");
        eImgs[1] = loadImage("enemy0.png");
        eType = 1;
    }
    public void draw()
    {
        if(eVisible)
        {
            // enemy animation
            if(millis()/200%2==1)
            {
                //leftside or rightside
                if(myPlayer.getX()<x)
                {
                    pushMatrix();
                    translate(x,y);
                    imageMode(CENTER);
                    scale(-1,1);
                    image(eImgs[0],0,0,w,h);
                    popMatrix();
                } else {
                    imageMode(CENTER);
                    image(eImgs[0],x,y,w,h);
                }
            } else {
                //leftside or rightside
                if(myPlayer.getX()<x)
                {
                    pushMatrix();
                    translate(x,y);
                    imageMode(CENTER);
                    scale(-1,1);
                    image(eImgs[1],0,0,w,h);
                    popMatrix();
                } else {
                    imageMode(CENTER);
                    image(eImgs[1],x,y,w,h);
                } 
            }
            fill(0);
            rectMode(CORNER);
            rect(x-25,y-40,50,8);
            fill(200,0,0);
            rectMode(CORNER);
            rect(x-24,y-39,50*(PApplet.parseFloat(eHealth)/100),6);
        }
    }
}

class Mutallisk extends Enemy
{
    Mutallisk(float x, float y,float w, float h, float eSpeed, int eDamage, int eHealth)
    {
        super(x,y,w,h,eSpeed,eDamage,eHealth);
        eImgs = new PImage[2];
        eImgs[0] = loadImage("enemy2.png");
        eImgs[1] = loadImage("enemy3.png");
        eType = 2;
    }
    public void draw()
    {
        if(eVisible)
        {
            // enemy animation
            if(millis()/300%2==1)
            {
                //leftside or rightside
                if(myPlayer.getX()>x)
                {
                    pushMatrix();
                    translate(x,y);
                    imageMode(CENTER);
                    scale(-1,1);
                    image(eImgs[0],0,0,w,h);
                    popMatrix();
                } else {
                    imageMode(CENTER);
                    image(eImgs[0],x,y,w,h);
                }
            } else {
                //leftside or rightside
                if(myPlayer.getX()>x)
                {
                    pushMatrix();
                    translate(x,y);
                    imageMode(CENTER);
                    scale(-1,1);
                    image(eImgs[1],0,0,w,h);
                    popMatrix();
                } else {
                    imageMode(CENTER);
                    image(eImgs[1],x,y,w,h);
                } 
            }
            
            fill(0);
            rectMode(CORNER);
            rect(x-25,y-40,50,8);
            fill(200,0,0);
            rectMode(CORNER);
            rect(x-24,y-39,50*(PApplet.parseFloat(eHealth)/300),6);
        }
    }
}

//mutallisk shoot Bug when player approaches
class Bug extends Enemy
{
    int i = 0;
    Bug(float x, float y,float w, float h, float eSpeed, int eDamage, int eHealth)
    {
        super(x,y,w,h,eSpeed,eDamage,eHealth);
        eImgs = new PImage[1];
        eImgs[0] = loadImage("enemy4.png");
        eType = 3;
    }
    public void draw()
    {
        if(eVisible)
        {
            //rotate while moving
            pushMatrix();
            translate(x,y);
            imageMode(CENTER);
            rotate(-i/10);
            image(eImgs[0],0,0,w,h);
            popMatrix();
            i++;


            fill(0);
            rectMode(CORNER);
            rect(x-25,y-35,50,8);
            fill(200,0,0);
            rectMode(CORNER);
            rect(x-24,y-34,50*(PApplet.parseFloat(eHealth)/100),6);
        }

    }
}
abstract class Item
{
    protected float x,y,w,h;
    protected boolean iVisible;
    protected PImage iImg;
    protected int iType;

    Item(float x, float y, float w, float h)
    {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        iVisible = true;
    }
    public void draw()
    {
        if(iVisible)
        {
            pushMatrix();
            translate(x,y);
            imageMode(CENTER);
            rotate(radians(45));
            image(iImg,0,0,w,h);
            popMatrix();
        }
    }
    public void setVisible(boolean a)
    {
        iVisible = a;
    }
    public boolean getVisible(){return iVisible;}
    public float getX(){return x;}
    public float getY(){return y;}
    public float getW(){return w;}
    public float getH(){return h;}
    public int getType(){return iType;}
}

class ItemDynamite extends Item
{
    ItemDynamite(float x, float y, float w, float h)
    {
        super(x,y,w,h);
        iImg = loadImage("bullet2.png");
        iType = 2;
    }
}
class ItemShark extends Item
{
    ItemShark(float x, float y, float w, float h)
    {
        super(x,y,w,h);
        iImg = loadImage("bullet4.png");
        iType = 3;
    }
    public void draw()
    {
        if(iVisible)
        {
            pushMatrix();
            translate(x,y);
            imageMode(CENTER);
            rotate(radians(225));
            image(iImg,0,0,w,h);
            popMatrix();
        }
    }
}
class Player
{
  float pSpeed;
  PImage[] pImgs;
  int pDirection;
  int pHealth;
  float x,y,w,h;
  boolean pLeft,pRight,pUp,pDown = false;
 
  Player(float x,float y, float width, float height, int speed, int health)
  {
    pImgs = new PImage [8];
    pSpeed = speed;
    pHealth = health;
    for(int i=0; i<8; i++)
    {
    pImgs[i] = loadImage("player"+i+".png");
    }
    w = width;
    h= height;
    pDirection = 2;
    
    this.x = x;
    this.y = y;
  }

  public float getX(){return x;}
  public float getY(){return y;}
  public float getW(){return w;}
  public float getH(){return h;}
  public int getHealth(){return pHealth;}
  public int getDirection(){return pDirection;}
  public void setSpeed(int a){pSpeed = a;}
  public void setHealth(int a){pHealth = a;}
  public void setDirection()
  {
    if(pLeft)
    {
      if(pUp) pDirection = 7;
      else if(pDown) pDirection = 5;
      else pDirection = 6;
    }
    else if(pRight)
    {
      if(pUp) pDirection = 1;
      else if(pDown) pDirection = 3;
      else pDirection = 2;
    }
    else if(pUp) pDirection = 0;
    else if(pDown) pDirection = 4;
  }

  public void updatePositionAndDraw()
  {
    move();
    setDirection();
    imageMode(CENTER);
    image(pImgs[pDirection],x,y,w,h);
    fill(0);
    rectMode(CORNER);
    rect(x-25,y+40,50,10);
    if(pHealth <100)
    fill(255,0,0);
    else fill(255,255,0);
    rectMode(CORNER);
    rect(x-24,y+41,48*(PApplet.parseFloat(pHealth)/200),8);
  }
  public void move()
  {
    if(pLeft)
      if(myPlayer.getX()>30) x -= pSpeed;
    if(pRight)
      if(myPlayer.getX()<770) x += pSpeed;
    if(pUp)
      if(myPlayer.getY()>40) y -= pSpeed;
    if(pDown)
      if(myPlayer.getY()<610) y += pSpeed;
  }

}
  public void settings() {  size(800,800); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Java_Code" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
