abstract class Enemy
{
    protected int eDamage,eHealth;
    protected boolean eVisible;
    protected PImage[] eImgs;
    protected float eSpeed,x,y,w,h;
    protected int eType;
    protected PVector eDirection;

    Enemy(float x, float y,float w, float h, float eSpeed, int eDamage,int eHealth)
    {
        eVisible = true;
        this.x = x;
        this.y = y;
        this.w= w;
        this.h = h;
        this.eDamage = eDamage;
        this.eSpeed = eSpeed;
        this.eHealth = eHealth;
    }
    abstract void draw();
    int getHealth(){return eHealth;}
    float getX(){return x;}
    float getY(){return y;}
    float getW(){return w;}
    float getH(){return h;}
    int getDamage(){return eDamage;}
    float getSpeed(){return eSpeed;}
    boolean getVisible(){return eVisible;}
    void setVisible(boolean tf){eVisible = tf;}
    int getType(){return eType;}
    void setType(int a){eType = a;}
    void setDirection(float x,float y)
    {
        eDirection = new PVector(x,y);
        eDirection.normalize();
    }
    PVector getDirection(){return eDirection;}
}

//Enemies are created at once when each round starts.
class EnemyFactory
{
  Zergling createZergling(int x, int y)
  {
    Zergling e = new Zergling(x,y,50,50,1,10,100);
    enemyNumber +=1;
    return e;
  }
  Mutallisk createMutallisk(int x, int y)
  {
    Mutallisk e = new Mutallisk(x,y,60,60,1,7,200);
    enemyNumber +=1;
    return e;
  }
}

class Zergling extends Enemy
{
    Zergling(float x, float y,float w, float h, float eSpeed, int eDamage, int eHealth)
    {
        super(x,y,w,h,eSpeed,eDamage,eHealth);
        eImgs = new PImage[2];
        eImgs[0] = loadImage("enemy1.png");
        eImgs[1] = loadImage("enemy0.png");
        eType = 1;
    }
    void draw()
    {
        if(eVisible)
        {
            // enemy animation
            if(millis()/200%2==1)
            {
                //leftside or rightside
                if(myPlayer.getX()<x)
                {
                    pushMatrix();
                    translate(x,y);
                    imageMode(CENTER);
                    scale(-1,1);
                    image(eImgs[0],0,0,w,h);
                    popMatrix();
                } else {
                    imageMode(CENTER);
                    image(eImgs[0],x,y,w,h);
                }
            } else {
                //leftside or rightside
                if(myPlayer.getX()<x)
                {
                    pushMatrix();
                    translate(x,y);
                    imageMode(CENTER);
                    scale(-1,1);
                    image(eImgs[1],0,0,w,h);
                    popMatrix();
                } else {
                    imageMode(CENTER);
                    image(eImgs[1],x,y,w,h);
                } 
            }
            fill(0);
            rectMode(CORNER);
            rect(x-25,y-40,50,8);
            fill(200,0,0);
            rectMode(CORNER);
            rect(x-24,y-39,50*(float(eHealth)/100),6);
        }
    }
}

class Mutallisk extends Enemy
{
    Mutallisk(float x, float y,float w, float h, float eSpeed, int eDamage, int eHealth)
    {
        super(x,y,w,h,eSpeed,eDamage,eHealth);
        eImgs = new PImage[2];
        eImgs[0] = loadImage("enemy2.png");
        eImgs[1] = loadImage("enemy3.png");
        eType = 2;
    }
    void draw()
    {
        if(eVisible)
        {
            // enemy animation
            if(millis()/300%2==1)
            {
                //leftside or rightside
                if(myPlayer.getX()>x)
                {
                    pushMatrix();
                    translate(x,y);
                    imageMode(CENTER);
                    scale(-1,1);
                    image(eImgs[0],0,0,w,h);
                    popMatrix();
                } else {
                    imageMode(CENTER);
                    image(eImgs[0],x,y,w,h);
                }
            } else {
                //leftside or rightside
                if(myPlayer.getX()>x)
                {
                    pushMatrix();
                    translate(x,y);
                    imageMode(CENTER);
                    scale(-1,1);
                    image(eImgs[1],0,0,w,h);
                    popMatrix();
                } else {
                    imageMode(CENTER);
                    image(eImgs[1],x,y,w,h);
                } 
            }
            
            fill(0);
            rectMode(CORNER);
            rect(x-25,y-40,50,8);
            fill(200,0,0);
            rectMode(CORNER);
            rect(x-24,y-39,50*(float(eHealth)/300),6);
        }
    }
}

//mutallisk shoot Bug when player approaches
class Bug extends Enemy
{
    int i = 0;
    Bug(float x, float y,float w, float h, float eSpeed, int eDamage, int eHealth)
    {
        super(x,y,w,h,eSpeed,eDamage,eHealth);
        eImgs = new PImage[1];
        eImgs[0] = loadImage("enemy4.png");
        eType = 3;
    }
    void draw()
    {
        if(eVisible)
        {
            //rotate while moving
            pushMatrix();
            translate(x,y);
            imageMode(CENTER);
            rotate(-i/10);
            image(eImgs[0],0,0,w,h);
            popMatrix();
            i++;


            fill(0);
            rectMode(CORNER);
            rect(x-25,y-35,50,8);
            fill(200,0,0);
            rectMode(CORNER);
            rect(x-24,y-34,50*(float(eHealth)/100),6);
        }

    }
}