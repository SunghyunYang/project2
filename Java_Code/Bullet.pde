
abstract class Bullet
{
    protected int bDamage; 
    protected int bDirection;
    protected boolean bVisible;
    protected PImage bImg;
    protected float damper,bSpeed,x, y,w,h;
    protected int bType;
    protected int count;

    Bullet(float x, float y,float w, float h,int bDirection, float bSpeed, int bDamage){
        bVisible = true;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.bDirection = bDirection;
        this.bDamage = bDamage;
        this.bSpeed = bSpeed;
    }
    void draw()
    {
        if(bVisible ==true)
        {
            pushMatrix();
            translate(x,y);
            imageMode(CENTER);
            rotate(radians(45*bDirection));
            image(bImg,0,0,w,h);
            popMatrix();
        }
    }
    void setVisible(boolean tf){bVisible = tf;}
    boolean getVisible(){return bVisible;}
    float getSpeed(){return bSpeed;}
    float getX(){return x;}
    float getY(){return y;}
    float getW(){return w;}
    float getH(){return h;}
    float getDamper(){return damper;}
    int getDamage(){return bDamage;}
    int getDirection(){return bDirection;}
    int getType(){return bType;}
}

class Bullet1 extends Bullet
{
    Bullet1(float x, float y,float  w, float h, int bDirection, float bSpeed, int bDamage)
    {
        super(x,y,w,h,bDirection,bSpeed,bDamage);
        bImg = loadImage("bullet1.png");
        damper = 0;
        bType = 1;
    }
}

class Dynamite extends Bullet
{
    Dynamite(float x, float y,float w, float h, int bDirection, float bSpeed, int bDamage)
    {
        super(x,y,w,h,bDirection,bSpeed,bDamage);
        bImg = loadImage("bullet2.png");
        damper = 0.2;
        bType = 2;
    }
    void draw()
    {
        if(bVisible ==true)
        {
            pushMatrix();
            translate(x,y);
            imageMode(CENTER);
            rotate(radians(45*(bDirection+1)));
            image(bImg,0,0,w,h);
            popMatrix();
        }
        
    }    
}

class Explosion extends Bullet
{
    Explosion(float x, float y,float w, float h, int bDirection, float bSpeed, int bDamage)
    {
        super(x,y,w,h, bDirection,bSpeed,bDamage);
        bImg = loadImage("bullet3.png");
        damper = 0;
        bType = 3;
        count = 100;
    }
    void draw()
    {
        if(bVisible == true)
        {
            pushMatrix();
            translate(x,y);
            imageMode(CENTER);
            image(bImg,0,0,w,h);
            popMatrix();
        }
	}
    
}

class Shark extends Bullet
{
    Shark(float x, float y,float w, float h, int bDirection, float bSpeed, int bDamage)
    {
        super(x,y,w,h, bDirection,bSpeed,bDamage);
        bImg = loadImage("bullet4.png");
        damper = 0;
        bType = 4;
    }
}