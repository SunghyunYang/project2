# Project 2 template #

This is the template for your project 2 submission. For a full description of the Project, please refer to [this document](https://docs.google.com/document/d/1jmFpRdEZJ_Cmk9Hp1q_I7Co9vRo-d5a3fUHcKBYSAPk/edit?usp=sharing).

## Personal information to add

* **Name**: Sunghyun Yang  
* **Student ID**: 20150459
* **email**: yp737@kaist.ac.kr

---
## Marine vs Zerg

![](Images/gameplay.png)
This game is marine vs zerg. You can control the player with keyboard. It’s a game that kills countless zergs and get the highest score.

---
## How to Play

* To control the player, press arrow keys to move.
* Press "Z" key to shoot bullet
* To kill enemies effectively, use special bullets
* you can throw dynamite with "X" key, and shoot shark bullet wih "C" key.
* If you kill enemy, you can earn points and money.
* Upgrade player, and buy bullets at Command Center



---
## Processing Architecture
### Overall UML
![](Images/UML.png)

### Bullets
![](Images/Bullets.png)

### Enemies
![](Images/Enemies.png)

### Command Center
![](Images/commandcenter.png)

* Fill Player's health
* Upgrade Player's Speed
* Upgrade Bullet Damage
* Buy Dynamite
* Buy Shark bullet
---
## Dependencies Used

### Processing 

*Sprite library* 

* `setFrameSequence(int firstFrame, int lastFrame, double interval)` Sets upt the animation sequence
* `setXY` Set the sprite location
* `new Sprite(this, "image.png", int row, int column, 1)' Load image.png, separate image with rows/colums

*Minim library* 

* Play effect sounds such as gunshot, item acquired, 

*Websocket library*

* Update gameInfo JSON object, and send it to client with `.sendMessage(Object)`

![](Images/gameInfo.png)


### P5.js

*p5.clickable.js*

* Setup and draw buttons
* use `.onHover`, `.onOutside`, `.onPress`, `.onRelease` functions to control the button.
* use `.cornerRadius`, `.strokeWeight`, `.textFont`, `.color`, etc to modify the button.

*p5.scribble.js*

* Draw 2D primitives in a sketchy look.
* use `.onHover`, `.onOutside`, `.onPress`, `.onRelease` functions to control the button.
* use `.cornerRadius`, `.strokeWeight`, `.textFont`, `.color`, etc to modify the button.

*Sprite Class*

* on p5.js, I create sprite class instead of using sprite library, because I already use sprite library on processing file.
![](Images/spriteClass.png)
![](Images/spriteClass2.png)
![](Images/speedspritesheet.png)

*Websocket*

* Update gameInfo JSON object, and send it to server with `.send(Object)`

--- 

## How two projects communicate

![](Images/communication.png)


--- 

## References

* [p5.clickable.js](https://github.com/Lartu/p5.clickable)
* [p5.scribble.js](https://github.com/generative-light/p5.scribble.js)
* [sprite class](https://www.youtube.com/watch?v=3noMeuufLZY)
* [sprite library](https://www.youtube.com/watch?v=xvjrsBC9Vnc)
